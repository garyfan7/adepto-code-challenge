# Adepto Code Challenge - Flickr Interesting Images

## Overview
This project is a simple code challenge provided by Adepto Code Challenge, it is an opportunity for me, as a candidate, to demonstrate my problem solving, software design and engineering skills. The aim of this exercise is to simulate real working conditions to provide context for a code/design review during the follow up interview.

## The Task
Create a service that interacts with the Flickr public API based on the following user stories:

* As a User I want to be able to see a set of Interesting images so that I can get some visual inspiration (please use the flickr.interestingness.getList API for this purpose).
* As a User I want to be able to click on a specific image in the "interesting set" so that I can see a full version of the image and additional information about that image.

## Tech Stack

This project is built using the following technologies:
- Node.js 
- Express
- React

While this project could have been possible with a simple React frontend this would come with security risks as the API key would have needed to be stored somewhere on the front end. For this reason I have created a simple API using node that receives requests from React and grabs data from the Flickr API.

## Built With, Technical and architectural choices
* This project is built with ```React``` by bootstrapping ```create-react-app``` and coded with ES6 Javascript standard.
* Components have been broken down as small as possible for the sake of readability, scalability and testability.
* React Router handles all the urls and browser history
* Simple helper ```DateFormatter``` and ```ViewsFormatter``` are built as a module to demostrate the developer is capable with building reusable code to be shared among all the components in the project. 
* For small amount and simple data set in this project, in my opinion, state management library like ```redux``` or ```mbox``` are definitely overkill. However, it can be added if required. 
* Jest is used for testing purpose, however, only some tests are written due to time constraint.
* ```prop-types``` is used for runtime type checking.
* ```node-fetch``` for making HTTP requests with Node.js

## Server Setup

The server is setup using express. The `routes/api` folder contains the different endpoints used in this App:
- getPhotoDetail;
- getPhotos;

Each of these endpoints make a different sort of request to the Flickr API. The API key is stored as a local environment variable. I have used a libary called `node-fetch` to handle making API calls on the server side. This library allows the use of the fetch API on the server side, which keeps things consistent with the front end. I've also made use of `async/await` to keep the code nice and clean (and make use of some shiny new ES6!).

## Choices of styling
Normally, the way to style component should be consistent among the entire project. However, both Materialize css & Styled Component from emotion are used for demostrating the developer has the ability to work with CSS library as well as styled componement for styling.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Loads environment variables

You have to create a ```.env``` file to store the API key in you root directory. For example 

```CLIENT_KEY=123123123```

### Downloading and Installing

1. Download the package, or run `git clone https://garyfan7@bitbucket.org/garyfan7/adepto-code-challenge.git`
2. run `yarn` to install packages.
3. ```cd``` to client and run ```yarn``` to install packages.
4. ```cd``` to root dir and run `yarn start` to run adepto-code-challenge on Local server

## Running the tests

run `yarn test` to run all test case in watch mode. Press `u` to update snapshot if needed.

## Authors

* **Gary Fan** - [Personal Website](https://garyfan.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details