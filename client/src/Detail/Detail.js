import React, { Component } from "react";
import styled from "react-emotion";
import DateFormatter from "../module/DateFormatter";

const ArrowIconWrapper = styled("div")`
  position: absolute;
  top: 20px;
  left: 30px;
  z-index: 1 !important;
  cursor: pointer;
  
  :hover {
    transition: 1s;  
    opacity: .7;
    transform: translateX(-20%);
    -webkit-transform: translateX(-20%);
  }
`;

const DetailWrapper = styled("div")`

  .poster-style {
    margin-top: -50px;
    z-index: 1 !important;

    img {
      position: relative;
      border-radius: 10px;
      box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.5),
        0px 8px 16px rgba(0, 0, 0, 0.5), 0px 16px 32px rgba(0, 0, 0, 0.5);
    }
  }
`;

const OverviewSection = styled("div")`

  padding: 10px;
  border-top: 1px solid #0F303D;

  .overview-title {
    font-family: Montserrat;
    font-weight: bold;
    font-size: 2em;
    letter-spacing: 2px;
    color: #e3f4fc;
  }

  .overview-text {
    font-family: "Roboto";
    color: #9FBBC7;
    display: block;
    padding: 10px 0;
    font-size: 1.2em;
  }
`;

const BackdropWrapper = styled("div")(
  ({ backdrop }) => `
    filter: blur(4px);
    -webkit-filter: blur(4px);
    content: '';
    left: 0;
    right: 0;
    height: 450px;
    width: 100%;
    display: block;
    z-index: 0;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: 50% 50%;
    background-image: url(${backdrop});
    will-change: opacity;
    transition: filter 1s;
`
);

const TitleContent = styled("div")`
  padding-top: 10%;
  height: 100%;
  font-family: Montserrat;
  flex: 1;

  .photo-title {
    display: block;
    font-weight: bold;
    font-size: 2em;
    color: #e3f4fc;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
  }

  .photo-detail {
    box-sizing: border-box;
    display: block;
    font-size: 1.1em;
    color: #b8d8e6;
  }
`;

class Detail extends Component {
  state = {
    photo: null
  };
  componentDidMount() {
    let photo_id = this.props.match.params.photo_id;
    this.getPhotoDetail(photo_id)
    .then(res => {
        this.setState({
          photo: res.response.photo
        })
    })
    .catch(err => console.log(err));
  }

  getPhotoDetail = async (photo_id) => {
      const response = await fetch(`/api/getPhotoDetail/?photo_id=${photo_id}`)
      const body = await response.json();
      if (response.status !== 200) throw Error(body.message);
      return body;
  }

  render() {
    let photo = this.state.photo;
    let url = photo ? "https://farm" + photo.farm + ".staticflickr.com/" + photo.server + "/" + photo.id + "_" + photo.secret : "";
    
    const BackToHome = () => {
      this.props.history.goBack();
    }

    const photoDetail = photo ? (
      <React.Fragment>
        <ArrowIconWrapper>
          <i className="medium material-icons white-text" onClick={() => BackToHome()}>arrow_back</i>
        </ArrowIconWrapper>
        <BackdropWrapper backdrop={ url + "_c.jpg" } />
        <div className="row container">
          <div className="col m5 s12 poster-style">
            <img
              className="responsive-img"
              alt={photo.title}
              src={url + ".jpg"}
            />
          </div>
          <div className="col m7 s12">
            <TitleContent>
              <span className="photo-title">
                {photo.title._content}
              </span>
              <span className="photo-detail">
                {photo.owner.realname} - {DateFormatter(photo.dates.taken, "YYYY-MM-DD hh:mm:ss", "MMMM YYYY")}
              </span>

            </TitleContent>
          </div>
        </div>
        <div className="row container">
          <OverviewSection>
            <span className="overview-title"> Description </span>
            <span className="overview-text"> {photo.description._content} </span>
          </OverviewSection>
        </div>

      </React.Fragment>
    ) : (
        <div className="center">
          <h3 className="white-text"> Loading photo...</h3>
        </div>
      );

    return <DetailWrapper>{photoDetail}</DetailWrapper>;
  }
}

export default Detail;
