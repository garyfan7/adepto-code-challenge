import React, { Component } from "react";
import { Link } from "react-router-dom";
import PhotoCard from "./components/PhotoCard";
import SearchBar from "./components/SearchBar";
import styled from "react-emotion";
import Background from "../assets/hero-background.svg";
import Logo from "../assets/Flickr_logo.png";

const SectionHeader = styled("div")`
  font-family: Montserrat;
  font-style: normal;
  font-weight: bold;
  line-height: 24px;
  font-size: 34px;
  color: #e3f4fc;
  margin: 30px 10px;
`;

const HeroSection = styled("div")`
  background-position: -150px -460px;
  background-image: url(${Background});

  img {
    width: 150px;
    padding: 100px 0;
  }
`;

class Home extends Component {
  state = {
    photos: [],
    filteredPhotos: [],
    currentPage: 1
  };

  componentDidMount() {
    // Fetching photos data from thephotodb api by making a HTTP using axios
      this.getPhotos(1)
      .then(res => {
          this.setState({
            photos: res.response.photos.photo,
            filteredPhotos: res.response.photos.photo,
          })
      })
      .catch(err => console.log(err));
  }

  getPhotos = async (pagenum) => {
      const response = await fetch(`/api/getPhotos/?pagenum=${pagenum}`)
      const body = await response.json();
      if (response.status !== 200) throw Error(body.message);
      return body;
  }

  handleChange = keyword => {
    let tempPhotoHolder = this.state.photos.filter(photo => {
      // Lower case all the string to ensure consistency
      let title = photo.title.toLowerCase();
      keyword = keyword.toLowerCase();
      return title.indexOf(keyword) >= 0;
    });

    this.setState({
      // Store the filtered data on local state
      filteredPhotos: tempPhotoHolder
    });
  };

  prevOrNextPage = (prevOrNext) => {

    let page;

    if (prevOrNext === "prev") {
      // Check if it is not the first page, set the current page to last page
      this.state.currentPage > 1 ? page = this.state.currentPage - 1 : page = 1;
    } else if (prevOrNext === "next") {
      // Check if it is not the first page, set the current page to next page
      this.state.currentPage < 500 ? page = this.state.currentPage + 1 : page = 500;
    }

    // Empty the photo list to tigger loading message
    this.setState({
      currentPage: page,
      photos: [],
      filteredPhotos: [],
    });

    this.getPhotos(page)
    .then(res => {
        this.setState({
          photos: res.response.photos.photo,
          filteredPhotos: res.response.photos.photo,
        })
    })
    .catch(err => console.log(err));
  }

  render() {
    const { filteredPhotos } = this.state;
    const photosList = filteredPhotos.length ? (
      filteredPhotos.map(photo => {
        return (
          <Link to={"/photo/" + photo.id} key={photo.id}>
            <PhotoCard photo={photo} />
          </Link>
        );
      })
    ) : (
        <div className="center">
          <h4 className="white-text">Loading interesting photos....</h4>
        </div>
      );
    return (
      <React.Fragment>
        <HeroSection>
          <div className="center">
            <img src={Logo} alt="Logo" />
            <div className="container">
              <SearchBar placeholder="Search" onChange={this.handleChange} />
            </div>
          </div>
        </HeroSection>
        <div className="container">
          <SectionHeader>Interesting photos</SectionHeader>
          <div className="row">{photosList}</div>
          <div className="row">
            {this.state.currentPage > 1 && (
              <div onClick={() => this.prevOrNextPage("prev")} className="waves-effect waves-light pink btn left">
                <i className="material-icons left">keyboard_arrow_left</i>Prvious Page</div>
            )}
            {this.state.currentPage <= 500 && (
              <div onClick={() => this.prevOrNextPage("next")}
                className="waves-effect waves-light pink btn right">
                <i className="material-icons right">keyboard_arrow_right</i>Next Page</div>
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default Home;
