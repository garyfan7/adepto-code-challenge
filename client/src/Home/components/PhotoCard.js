import React from "react";
import styled from "react-emotion";
import DateFormatter from "../../module/DateFormatter";
import ViewsFormatter from "../../module/ViewsFormatter"

const PhotoCardWrapper = styled("div")`
    .card {
      background-color: transparent;
    }

    img {
      position: relative;
      border-radius: 20px !important;
      box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.5),
        0px 8px 16px rgba(0, 0, 0, 0.5), 0px 16px 32px rgba(0, 0, 0, 0.5);

      &:hover {
        opacity: .7;
        transition: .8s;
        transform: scale(1.05);
      }
    }

    .view-label {
      font-family: Roboto;
      font-weight: bold;
      font-size: 1em;
      text-align: center;
      position: absolute;
      z-index: 2;
      top: 10px;
      left: 10px;
      padding: 5px 15px;
      color: #fff;
      background-color: #fe0083;
      border-radius: 8px;
    }
`;

const ContentWrapper = styled("div")`
  padding: 6px 10px 10px;
  border-radius: 0 0 2px 2px;
  background-color: #081c24;

  .photo-title {
    text-overflow: ellipsis;
    display: block;
    overflow: hidden;
    white-space: nowrap;
    font-weight: 600;
    font-size: 1.2em;
    padding-top: 10px;
    font-family: Roboto;
    font-style: normal;
    font-weight: normal;
    color: #e6f7ff;
  }

  .photo-date {
    font-family: Roboto;
    font-style: normal;
    font-weight: normal;
    font-size: 0.9em;
    color: #a1d1e6;
  }
`;

const PhotoCard = ({ photo }) => {
  return (
    <PhotoCardWrapper>
      <div className="col s12 l3 m6">
        <div className="card z-depth-0">
          <span className="view-label">
            {ViewsFormatter(photo.views)} views
          </span>
          <div className="card-image">
            <img alt={photo.title}
              src={photo.url_q}/>
          </div>
          <ContentWrapper>
            <span className="photo-title">{photo.title}</span>
            <span className="photo-date">
              {DateFormatter(photo.datetaken, "YYYY-MM-DD hh:mm:ss", "MMMM YYYY")}
            </span>
          </ContentWrapper>
        </div>
      </div>
    </PhotoCardWrapper>
  );
};

export default PhotoCard;
