import React from "react";
import PhotoCard from "./PhotoCard";
import renderer from "react-test-renderer";

const photo = {
  id: 1,
  title: "Example photo"
}

test("Can Create PhotoCard", () => {
  expect(<PhotoCard/>).toBeDefined();
});

test("Renders correctly", () => {
  const tree = renderer
    .create(<PhotoCard photo={{}}/>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

test("Renders photo card correctly", () => {
  const tree = renderer
    .create(<PhotoCard photo={photo}/>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});