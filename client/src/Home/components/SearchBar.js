import React from "react";
import PropTypes from "prop-types";
import styled from "react-emotion";

const SearchBarWrapper = styled("div")`
    position: relative;
    display: flex;
    align-items: center;
    padding: 10px;
    font-size: 14px;
    line-height: 20px;

    .input-style {
        border: none;
        text-align: left;
        font-size: 14px;
        height: 4rem !important;
        padding-left: 30px !important;
        background: #FFFFFF !important;
        box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.5) !important;
        border-radius: 24px !important;
        border: 2px solid #fff !important;

        :focus, :hover {
            border-color: #fe0083 !important;
            transition: .5s;
            cursor: pointer;
        }

        ::placeholder {
            color: #fe0083;
            font-size: 1.2em;
            font-weight: 600;
            line-height: 16px;
            font-family: Roboto;
            font-style: normal;
        }
    }
`;

const SearchIconWrapper = styled("div")`
  position: absolute;
  right: 35px;
  color: #fe0083;
`;

const SearchBar = (props) => (
  <SearchBarWrapper>
    <input
      className="input-style"
      type="search"
      placeholder={props.placeholder}
      onChange={(event) => props.onChange(event.target.value)} />
    <SearchIconWrapper>
      <i className="small material-icons">search</i>
    </SearchIconWrapper>
  </SearchBarWrapper>
);

SearchBar.defaultProps = {
  placeholder: "Search..."
};

SearchBar.propTypes = {
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string
};

export default SearchBar;
