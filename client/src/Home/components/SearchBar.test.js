import React from "react";
import SearchBar from "./SearchBar";
import renderer from "react-test-renderer";

const fakeFunction = () => {
  return null;
}

test("Can Create SearchBar", () => {
  expect(<SearchBar onChange={fakeFunction}/>).toBeDefined();
});

test("Renders serach bar with props correctly", () => {
  const tree = renderer
    .create(<SearchBar placeholder="search" onChange={fakeFunction}/>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});