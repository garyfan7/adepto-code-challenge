import moment from "moment";
import PropTypes from "prop-types";

export const DateFormatter = (date, originalFormat, expectedFormat) => {
  return moment(date, originalFormat).format(expectedFormat);
}


DateFormatter.propTypes = {
  date: PropTypes.string.isRequired,
  originalFormat: PropTypes.string.isRequired,
  expectedFormat: PropTypes.string.isRequired,
};

export default DateFormatter;