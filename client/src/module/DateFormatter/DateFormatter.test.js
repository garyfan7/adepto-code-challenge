import DateFormatter from "./DateFormatter";

test("Can Create", () => {
  expect(DateFormatter("02/02/2019", "DD/MM/YYYY", "DD/MM/YY")).toBeDefined();
})

test("Can format correctly", () => {
  expect(DateFormatter("02/02/2019", "DD/MM/YYYY", "DD/MM/YY")).toBe("02/02/19");
})


