import PropTypes from "prop-types";

export const ViewsFormatter = (views) => {
	if(isNaN(views)) return views;

	if(views < 9999) {
		return views;
	}

	if(views < 1000000) {
		return Math.round(views/1000) + "K";
	}
	if( views < 10000000) {
		return (views/1000000).toFixed(2) + "M";
	}

	if(views < 1000000000) {
		return Math.round((views/1000000)) + "M";
	}

	if(views < 1000000000000) {
		return Math.round((views/1000000000)) + "B";
	}

	return "1T+";
}


ViewsFormatter.propTypes = {
  views: PropTypes.number.isRequired,
};

export default ViewsFormatter;