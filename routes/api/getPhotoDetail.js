const express = require("express");
const router = express.Router();
const fetch = require("node-fetch");
const dotenv = require("dotenv").config();

const client_key = process.env.CLIENT_KEY;

// Function for getting popular movies
async function getPhotoDetail(photo_id) {
  let response = await fetch(
    "https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=" + client_key + "&photo_id=" + photo_id + "&format=json&nojsoncallback=1"
  );
  let data = await response.json();
  return data;
}

// @route   GET api/getPhotoDetail
// @desc    Gets photo's detail from Flickr API https://www.flickr.com/services/api/

// Route for getting photo's detail

router.get("/", (req, res) => {
  const photo_id = req.query.photo_id;
  console.log(
    `Request recieved by server for photo detail, photo_id : ${req.query.photo_id}`
  );
  getPhotoDetail(photo_id).then(data => {
    res.send({ response: data });
  });
});

module.exports = router;