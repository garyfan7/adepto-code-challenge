const express = require("express");
const router = express.Router();
const fetch = require("node-fetch");
const dotenv = require("dotenv").config();

const client_key = process.env.CLIENT_KEY;

// Function for getting popular movies
async function getPhotos(currentPage) {
  let response = await fetch(
    "https://api.flickr.com/services/rest/?method=flickr.interestingness.getList&api_key=" + client_key + "&extras=url_q%2C+owner_name%2C+date_taken%2C+url_l%2C+views&per_page=8&page=" + currentPage + "&format=json&nojsoncallback=1"
  );
  let data = await response.json();
  return data;
}

// @route   GET api/getPhotos
// @desc    Gets interesting photos from Flickr API https://www.flickr.com/services/api/

// Route for getting interesting photos

router.get("/", (req, res) => {
  const pageNum = req.query.pagenum;
  console.log(
    `Request recieved by server for interesting photos, page ${req.query.pagenum}`
  );
  getPhotos(pageNum).then(data => {
    res.send({ response: data });
  });
});

module.exports = router;