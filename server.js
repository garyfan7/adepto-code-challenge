const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const app = express();

// Set up routes
const getPhotos = require("./routes/api/getPhotos");
const getPhotoDetail = require("./routes/api/getPhotoDetail");

// Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Routes
app.use("/api/getPhotos", getPhotos);
app.use("/api/getPhotoDetail", getPhotoDetail);


// Setting up local and live server configuration
const port = process.env.PORT || 5000;

if (process.env.NODE_ENV === "production") {
  // Serve any static files
  app.use(express.static(path.join(__dirname, "client/build")));

  // Handle React routing, return all requests to React app
  app.get("*", function (req, res) {
    res.sendFile(path.join(__dirname, "client/build", "index.html"));
  });
}

app.listen(port, () => console.log(`Listening on port ${port}`));
